package dirtempl_yaml

import (
	"github.com/goccy/go-yaml"
	"gitlab.com/Ma_124/dirtempl"
	"io"
)

// ReadConfig reads a DirConfig from r.
func ReadConfig(r io.Reader) (*dirtempl.DirConfig, error) {
	cfg := &dirtempl.DirConfig{}
	err := yaml.NewDecoder(r).Decode(cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}
