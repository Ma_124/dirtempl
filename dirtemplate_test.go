package dirtempl_test

import (
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Ma_124/dirtempl"
	"gitlab.com/Ma_124/dirtempl/engines/gotemplate/gotext-engine"
	"gopkg.in/yaml.v2"
	"os"
	"path/filepath"
	"testing"
)

const dirtemplConfigName = "_dirtempl.yml"

func TestExecute(t *testing.T) {
	t.Run("literal", func(t *testing.T) {
		t.Run("no files", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)
			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
		t.Run("single dir", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)

			mkdir(fs, `templ/dir`)
			defer expectDir(t, fs, "dir")

			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
		t.Run("single file", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)
			createFile(fs, `hello`, `world`)
			defer expectFile(t, fs, `hello`, `world`)

			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
		t.Run("tree", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)
			createFile(fs, `hello`, `world`)
			defer expectFile(t, fs, `hello`, `world`)

			createFile(fs, `foo`, `bar`)
			defer expectFile(t, fs, `foo`, `bar`)

			mkdir(fs, "templ/dir")
			createFile(fs, `dir/nest`, `ed`)
			defer expectFile(t, fs, `dir/nest`, `ed`)

			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
	})
	t.Run("config", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			t.Run("text", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createRawConfig(fs, "", "")
				defer notExpectFile(t, fs, dirtemplConfigName)
				createFile(fs, `hello`, `world`)
				defer expectFile(t, fs, `hello`, `world`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
				assert.NoError(t, err)
			})
			t.Run("object", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{})
				defer notExpectFile(t, fs, dirtemplConfigName)
				createFile(fs, `hello`, `world`)
				defer expectFile(t, fs, `hello`, `world`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
				assert.NoError(t, err)
			})
			t.Run("single noop entry", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{
					Files: map[string]dirtempl.FileEntry{
						"hello": {},
					},
				})
				defer notExpectFile(t, fs, dirtemplConfigName)
				createFile(fs, `hello`, `world`)
				defer expectFile(t, fs, `hello`, `world`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
				assert.NoError(t, err)
			})
		})
		t.Run("ignored file", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)
			createConfig(fs, "", &dirtempl.DirConfig{
				Files: map[string]dirtempl.FileEntry{
					"hello": {
						Ignored: true,
					},
				},
			})
			createFile(fs, `hello`, `world`)
			defer notExpectFile(t, fs, "hello")
			createFile(fs, `foo`, `bar`)
			defer expectFile(t, fs, `foo`, `bar`)

			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
		t.Run("moved file", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)
			createConfig(fs, "", &dirtempl.DirConfig{
				Files: map[string]dirtempl.FileEntry{
					"hello": {
						DestinationNames: []string{"salve"},
					},
				},
			})
			createFile(fs, `hello`, `world`)
			defer expectFile(t, fs, "salve", "world")
			defer notExpectFile(t, fs, "hello")
			createFile(fs, `foo`, `bar`)
			defer expectFile(t, fs, `foo`, `bar`)

			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
		t.Run("duplicated file", func(t *testing.T) {
			fs := afero.NewMemMapFs()
			mkdir(fs, `templ`)
			createConfig(fs, "", &dirtempl.DirConfig{
				Files: map[string]dirtempl.FileEntry{
					"hello": {
						DestinationNames: []string{"salve", "hallo"},
					},
				},
			})
			createFile(fs, `hello`, `world`)
			defer expectFile(t, fs, "salve", "world")
			defer expectFile(t, fs, "hallo", "world")
			defer notExpectFile(t, fs, "hello")
			createFile(fs, `foo`, `bar`)
			defer expectFile(t, fs, `foo`, `bar`)

			mkdir(fs, `dest`)

			err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{}, "templ", "dest")
			assert.NoError(t, err)
		})
		t.Run("templated config", func(t *testing.T) {
			t.Run("templated filename", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{
					Files: map[string]dirtempl.FileEntry{
						"hello": {
							DestinationNames: []string{"{{ .Hello }}"},
						},
					},
				})
				createFile(fs, `hello`, `world`)
				defer expectFile(t, fs, "salve", "world")
				defer notExpectFile(t, fs, "hello")
				createFile(fs, `foo`, `bar`)
				defer expectFile(t, fs, `foo`, `bar`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{"Hello": "salve"}, "templ", "dest")
				assert.NoError(t, err)
			})
			t.Run("templated dirname", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{
					Files: map[string]dirtempl.FileEntry{
						"hello": {
							DestinationNames: []string{"{{ .Hello }}"},
						},
					},
				})
				mkdir(fs, `templ/hello`)
				defer expectDir(t, fs, "salve")
				defer notExpectFile(t, fs, "hello")
				createFile(fs, `foo`, `bar`)
				defer expectFile(t, fs, `foo`, `bar`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{"Hello": "salve"}, "templ", "dest")
				assert.NoError(t, err)
			})
		})
		t.Run("templated", func(t *testing.T) {
			t.Run("file", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{
					Files: map[string]dirtempl.FileEntry{
						"hello": {
							TemplatedContent: "true",
						},
					},
				})
				createFile(fs, `hello`, `{{ .World }}`)
				defer expectFile(t, fs, "hello", "mundo")
				createFile(fs, `foo`, `bar`)
				defer expectFile(t, fs, `foo`, `bar`)
				createFile(fs, `baz`, `{{ .World }}`)
				defer expectFile(t, fs, `baz`, `{{ .World }}`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{"World": "mundo"}, "templ", "dest")
				assert.NoError(t, err)
			})
			t.Run("not templated file", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{
					Files: map[string]dirtempl.FileEntry{
						"hello": {
							TemplatedContent: "false",
						},
					},
				})
				createFile(fs, `hello`, `{{ .World }}`)
				defer expectFile(t, fs, "hello", "{{ .World }}")
				createFile(fs, `foo`, `bar`)
				defer expectFile(t, fs, `foo`, `bar`)
				createFile(fs, `baz`, `{{ .World }}`)
				defer expectFile(t, fs, `baz`, `{{ .World }}`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.Engine, map[string]interface{}{"World": "mundo"}, "templ", "dest")
				assert.NoError(t, err)
			})
			t.Run("implicitly file", func(t *testing.T) {
				fs := afero.NewMemMapFs()
				mkdir(fs, `templ`)
				createConfig(fs, "", &dirtempl.DirConfig{
					Files: map[string]dirtempl.FileEntry{
						"hello": {},
					},
				})
				createFile(fs, `hello.tmpl`, `{{ .World }}`)
				defer expectFile(t, fs, "hello", "mundo")
				createFile(fs, `foo`, `bar`)
				defer expectFile(t, fs, `foo`, `bar`)
				createFile(fs, `baz`, `{{ .World }}`)
				defer expectFile(t, fs, `baz`, `{{ .World }}`)

				mkdir(fs, `dest`)

				err := dirtempl.Walk(fs, gotext_engine.New(".tmpl"), map[string]interface{}{"World": "mundo"}, "templ", "dest")
				assert.NoError(t, err)
			})
		})
	})
}

func createFile(fs afero.Fs, name, content string) {
	f, err := fs.Create("templ/" + name)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, err = f.WriteString(content)
	if err != nil {
		panic(err)
	}
}

func createConfig(fs afero.Fs, dir string, cfg *dirtempl.DirConfig) {
	data, err := yaml.Marshal(cfg)
	if err != nil {
		panic(err)
	}

	createRawConfig(fs, dir, string(data))
}

func createRawConfig(fs afero.Fs, dir string, cfg string) {
	createFile(fs, filepath.ToSlash(filepath.Join(dir, dirtemplConfigName)), cfg)
}

func expectFile(t *testing.T, fs afero.Fs, name, content string) {
	data, err := afero.ReadFile(fs, "dest/"+name)
	assert.NoError(t, err)
	assert.Equal(t, content, string(data))
}

func expectDir(t *testing.T, fs afero.Fs, name string) {
	fi, err := fs.Stat("dest/" + name)
	if assert.NoError(t, err) {
		assert.True(t, fi.IsDir())
	}
}

func notExpectFile(t *testing.T, fs afero.Fs, name string) {
	_, err := fs.Stat("dest/" + name)
	if !os.IsNotExist(err) {
		assert.Fail(t, "file "+name+" exists")
		expectFile(t, fs, name, "")
	}
}

func mkdir(fs afero.Fs, name string) {
	err := fs.Mkdir(name, 0644)
	if err != nil {
		panic(err)
	}
}
