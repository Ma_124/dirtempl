package dirtempl

import (
	"github.com/spf13/afero"
	"io"
	"os"
	"path/filepath"
)

// Params are passed to TemplateEngines.
type Params interface{}

// A TemplateEngine must implement the FileTemplater, FilenameTemplater, IsTemplate and ConfigLoader interfaces.
type TemplateEngine interface {
	FileTemplater
	FilenameTemplater
	IsTemplate
	ConfigLoader
}

// FileTemplater exposes the function ExecuteFile which is used to execute file templates.
// Implementations must read the template from r and write the rendered results to w.
// It's the callee's  responsibility to close r and w.
type FileTemplater interface {
	ExecuteFile(r io.ReadCloser, w io.WriteCloser, params Params) error
}

// FilenameTemplater exposes the function ExecuteFilename which is used to execute filename templates.
// Implementations must use template from templ and return the rendered results.
type FilenameTemplater interface {
	ExecuteFilename(templ string, params Params) (string, error)
}

// IsTemplate exposes the function IsTemplate which is used to determine whether the file with filename name is a template.
// If it is not a template implementations must return an empty string. If it is they must return the default filename for the rendered result.
//
// Example:
// The Pongo2 template engine uses .p2 as a file extension. The pongo2 engine's implementation checks if this suffix is present
// and if present returns the file name with out the extension.
//
//   text-template.p2      --> text-template
//   text-template.html.p2 --> text-template.html
//   not-a-template.html   --> ""
type IsTemplate interface {
	IsTemplate(name string) string
}

// A ConfigLoader is responsible for loading DirConfigs.
// LoadConfig is called for every file with name ConfigName() found in the template tree.
// It must read the config from r and return it.
// Typically it also renders it as a template. All official engines perform themselves on the config and
// then parse it using gitlab.com/Ma_124/dirtempl/dirtempl_yaml
type ConfigLoader interface {
	ConfigName() string
	LoadConfig(r io.Reader, params Params) (*DirConfig, error)
}

// Execute executes the dirtempl found at origin and writes the results to dest. It calls the TemplateEngine t with params whenever required.
func Execute(t TemplateEngine, params Params, origin string, dest string) error {
	return Walk(afero.NewOsFs(), t, params, origin, dest)
}

// Walk performs exactly the same operations as Execute but on a afero.Fs instead of the OS file system.
func Walk(fs afero.Fs, t TemplateEngine, params Params, srcPath string, outPath string) error {
	fis, err := readDir(fs, srcPath)
	if err != nil {
		return err
	}

	cfg, err := readConfig(fs, t, params, srcPath)
	if err != nil {
		return err
	}

	for _, fi := range fis {
		if fi.Name() == t.ConfigName() {
			continue
		}

		path := filepath.Join(srcPath, fi.Name())

		e := entryByNames(cfg, fi.Name())
		if e.Ignored {
			continue
		}

		var wouldBeTemplatedContent bool
		var newNames []string

		if len(e.DestinationNames) != 0 {
			newNames = e.DestinationNames

			if !fi.IsDir() {
				wouldBeTemplatedContent = t.IsTemplate(fi.Name()) != ""
			}
		} else {
			if !fi.IsDir() {
				tmp := t.IsTemplate(fi.Name())
				if tmp != "" {
					newNames = []string{tmp}
					wouldBeTemplatedContent = true
				}
			}

			if len(newNames) == 0 {
				newName, err := t.ExecuteFilename(fi.Name(), params)
				if err != nil {
					return err
				}
				newNames = []string{newName}
			}
		}

		templatedContent, err := parseBool(e.TemplatedContent, wouldBeTemplatedContent)
		if err != nil {
			return err
		}

		for _, newName := range newNames {
			newPath := filepath.Join(outPath, newName)
			if templatedContent {
				err = executeFile(fs, t, params, path, newPath)
				if err != nil {
					return err
				}
			} else {
				err = copyFileOrDir(fs, t, params, fi, path, newPath)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func readConfig(fs afero.Fs, t TemplateEngine, params Params, path string) (*DirConfig, error) {
	f, err := fs.Open(filepath.Join(path, t.ConfigName()))
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}
	defer f.Close()

	return t.LoadConfig(f, params)
}

func executeFile(fs afero.Fs, t TemplateEngine, params Params, src string, dst string) error {
	w, err := fs.Create(dst)
	if err != nil {
		return err
	}
	defer w.Close()

	r, err := fs.Open(src)
	if err != nil {
		return err
	}
	defer r.Close()

	err = t.ExecuteFile(r, w, params)
	if err != nil {
		return &os.PathError{
			Op:   "execute template",
			Path: src,
			Err:  err,
		}
	}
	return nil
}
