package pongo2_engine

import (
	"bytes"
	"github.com/fatih/structs"
	"github.com/flosch/pongo2"
	"gitlab.com/Ma_124/dirtempl"
	"gitlab.com/Ma_124/dirtempl/dirtempl_yaml"
	"io"
	"io/ioutil"
	"reflect"
	"strings"
)

// Engine is a TemplateEngine that uses github.com/flosch/pongo2. The extension is .p2
var Engine = templ{false, []string{".p2"}}

type templ struct {
	Buffered bool

	Extensions []string
}

// New returns a TemplateEngine that uses github.com/flosch/pongo2. Unless specified in exts it does not handle the .p2 extension.
// You should probably use Engine.
func New(exts ...string) dirtempl.TemplateEngine {
	return templ{false, exts}
}

// NewBuffered returns a TemplateEngine that uses a buffered version of github.com/flosch/pongo2. Unless specified in exts it does not handle the .p2 extension.
func NewBuffered(exts ...string) dirtempl.TemplateEngine {
	return templ{true, exts}
}

// LoadConfig satisfies the ConfigLoader interface.
func (templ) LoadConfig(r io.Reader, params dirtempl.Params) (*dirtempl.DirConfig, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	p, err := pongo2.FromBytes(data)
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	err = p.ExecuteWriterUnbuffered(getContext(params), &buf)
	if err != nil {
		return nil, err
	}
	return dirtempl_yaml.ReadConfig(&buf)
}

// ConfigName satisfies the ConfigLoader interface.
func (templ) ConfigName() string {
	return "_dirtempl.yml"
}

// IsTemplate satisfies the IsTemplate interface.
func (t templ) IsTemplate(name string) string {
	if t.Extensions == nil {
		return ""
	}
	for _, e := range t.Extensions {
		if strings.HasSuffix(name, e) {
			return name[:len(name)-len(e)]
		}
	}
	return ""
}

// ExecuteFile satisfies the FileTemplater interface.
func (t templ) ExecuteFile(r io.ReadCloser, w io.WriteCloser, params dirtempl.Params) error {
	defer r.Close()
	defer w.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	r.Close()

	p, err := pongo2.FromBytes(data)
	if err != nil {
		return err
	}

	ctx := getContext(params)
	if t.Buffered {
		return p.ExecuteWriter(ctx, w)
	}
	return p.ExecuteWriterUnbuffered(ctx, w)
}

func getContext(params dirtempl.Params) (ctx pongo2.Context) {
	var ok bool
	if ctx, ok = params.(pongo2.Context); !ok {
		v := reflect.ValueOf(params)

		for v.Kind() == reflect.Ptr {
			v = v.Elem()
		}

		if v.Kind() != reflect.Struct {
			ctx = map[string]interface{}{
				"v": params,
			}
		} else {
			ctx = structs.Map(params)
		}
	}
	return
}

// ExecuteFilename satisfies the FilenameTemplater interface.
func (templ) ExecuteFilename(templ string, params dirtempl.Params) (string, error) {
	p, err := pongo2.FromString(templ)
	if err != nil {
		return "", err
	}

	return p.Execute(getContext(params))
}
