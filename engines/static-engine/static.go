package static_engine

import (
	"gitlab.com/Ma_124/dirtempl"
	"gitlab.com/Ma_124/dirtempl/dirtempl_yaml"
	"io"
	"strings"
)

// Template is a TemplateEngine that just passes inputs through without processing them.
var Template dirtempl.TemplateEngine = templ{}

type templ struct {
	Extensions []string
}

// New returns a TemplateEngine that just passes inputs through without processing them.
func New(exts ...string) dirtempl.TemplateEngine {
	return templ{exts}
}

// LoadConfig satisfies the ConfigLoader interface.
func (t templ) LoadConfig(r io.Reader, params dirtempl.Params) (*dirtempl.DirConfig, error) {
	return dirtempl_yaml.ReadConfig(r)
}

// ConfigName satisfies the ConfigLoader interface.
func (templ) ConfigName() string {
	return "_dirtempl"
}

// IsTemplate satisfies the IsTemplate interface.
func (t templ) IsTemplate(name string) string {
	if t.Extensions == nil {
		return ""
	}
	for _, e := range t.Extensions {
		if strings.HasSuffix(name, e) {
			return name[:len(name)-len(e)]
		}
	}
	return ""
}

// ExecuteFile satisfies the FileTemplater interface.
func (templ) ExecuteFile(r io.ReadCloser, w io.WriteCloser, params dirtempl.Params) error {
	defer r.Close()
	defer w.Close()

	_, err := io.Copy(w, r)
	return err
}

// ExecuteFilename satisfies the FilenameTemplater interface.
func (templ) ExecuteFilename(templ string, params dirtempl.Params) (string, error) {
	return templ, nil
}
