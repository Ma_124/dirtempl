package gotext_engine

import (
	"bytes"
	"gitlab.com/Ma_124/dirtempl"
	"gitlab.com/Ma_124/dirtempl/dirtempl_yaml"
	"gitlab.com/Ma_124/dirtempl/engines/gotemplate/internal/shared"
	"io"
	"io/ioutil"
	"strings"
	"text/template"
)

// Engine is a TemplateEngine that uses text/template.
var Engine dirtempl.TemplateEngine = templ{}

type templ struct {
	Extensions []string
}

// New returns a TemplateEngine that uses text/template.
func New(exts ...string) dirtempl.TemplateEngine {
	return templ{exts}
}

// LoadConfig satisfies the ConfigLoader interface.
func (templ) LoadConfig(r io.Reader, params dirtempl.Params) (*dirtempl.DirConfig, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	t, err := template.New(shared.Name(r)).Parse(string(data))
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	err = t.Execute(&buf, params)
	if err != nil {
		return nil, err
	}
	return dirtempl_yaml.ReadConfig(&buf)
}

// ConfigName satisfies the ConfigLoader interface.
func (templ) ConfigName() string {
	return "_dirtempl.yml"
}

// IsTemplate satisfies the IsTemplate interface.
func (t templ) IsTemplate(name string) string {
	if t.Extensions == nil {
		return ""
	}
	for _, e := range t.Extensions {
		if strings.HasSuffix(name, e) {
			return name[:len(name)-len(e)]
		}
	}
	return ""
}

// ExecuteFile satisfies the FileTemplater interface.
func (templ) ExecuteFile(r io.ReadCloser, w io.WriteCloser, params dirtempl.Params) error {
	defer r.Close()
	defer w.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	t, err := template.New(shared.Name(r)).Parse(string(data))
	if err != nil {
		return err
	}
	return t.Execute(w, params)
}

// ExecuteFile satisfies the FilenameTemplater interface.
func (templ) ExecuteFilename(templ string, params dirtempl.Params) (string, error) {
	t, err := template.New(templ).Parse(templ)
	if err != nil {
		return "", err
	}

	var buf bytes.Buffer
	err = t.Execute(&buf, params)
	if err != nil {
		return "", err
	}
	return buf.String(), err
}
