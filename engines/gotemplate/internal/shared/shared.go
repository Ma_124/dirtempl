package shared

import (
	"io"
)

type namer interface {
	Name() string
}

// Name returns the result of the Name() method of the Reader if available and returns "<template>" otherwise.
func Name(r io.Reader) string {
	if n, ok := r.(namer); ok {
		return n.Name()
	}
	return "<template>"
}
