package gohtml_engine

import (
	"gitlab.com/Ma_124/dirtempl"
	gotext_engine "gitlab.com/Ma_124/dirtempl/engines/gotemplate/gotext-engine"
	"gitlab.com/Ma_124/dirtempl/engines/gotemplate/internal/shared"
	"html/template"
	"io"
	"io/ioutil"
	"strings"
)

// Engine is a TemplateEngine that uses html/template for file templating and text/template for configs and filenames.
var Engine dirtempl.TemplateEngine = templ{}

type templ struct {
	Extensions []string
}

// New returns a TemplateEngine that uses html/template for file templating and text/template for configs and filenames.
func New(exts ...string) dirtempl.TemplateEngine {
	return templ{exts}
}

// LoadConfig satisfies the ConfigLoader interface.
// It is a wrapper around gotext_engine.Engine.LoadConfig.
func (templ) LoadConfig(r io.Reader, params dirtempl.Params) (*dirtempl.DirConfig, error) {
	// we dont want to escape HTML here so we use text/template
	return gotext_engine.Engine.LoadConfig(r, params)
}

// ConfigName satisfies the ConfigLoader interface.
func (templ) ConfigName() string {
	return "_dirtempl.yml"
}

// LoadConfig satisfies the IsTemplate interface.
func (t templ) IsTemplate(name string) string {
	if t.Extensions == nil {
		return ""
	}
	for _, e := range t.Extensions {
		if strings.HasSuffix(name, e) {
			return name[:len(name)-len(e)]
		}
	}
	return ""
}

// LoadConfig satisfies the FileTemplater interface.
func (templ) ExecuteFile(r io.ReadCloser, w io.WriteCloser, params dirtempl.Params) error {
	defer r.Close()
	defer w.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	t, err := template.New(shared.Name(r)).Parse(string(data))
	if err != nil {
		return err
	}
	return t.Execute(w, params)
}

// LoadConfig satisfies the FilenameTemplater interface.
// It is a wrapper around gotext_engine.Engine.ExecuteFilename.
func (templ) ExecuteFilename(templ string, params dirtempl.Params) (string, error) {
	// we dont want to escape HTML here so we use text/template
	return gotext_engine.Engine.ExecuteFilename(templ, params)
}
