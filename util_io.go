package dirtempl

import (
	"github.com/spf13/afero"
	"io"
	"os"
)

func copyFileOrDir(fs afero.Fs, t TemplateEngine, params Params, fi os.FileInfo, src string, dst string) error {
	if fi.IsDir() {
		err := fs.Mkdir(dst, 0755)
		if err != nil && !os.IsExist(err) {
			return err
		}

		return Walk(fs, t, params, src, dst)
	}
	return copyFile(fs, src, dst)
}

func copyFile(fs afero.Fs, src string, dst string) error {
	w, err := fs.Create(dst)
	if err != nil {
		return err
	}
	defer w.Close()

	r, err := fs.Open(src)
	if err != nil {
		return err
	}
	defer r.Close()

	_, err = io.Copy(w, r)
	return err
}

// readDir returns the same output as afero.ReadDir but without sorting the FileInfos
func readDir(fs afero.Fs, dirname string) ([]os.FileInfo, error) {
	f, err := fs.Open(dirname)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	list, err := f.Readdir(-1)
	if err != nil {
		return nil, err
	}

	return list, nil
}
