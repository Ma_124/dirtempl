package dirtempl

import (
	"errors"
	"strconv"
	"strings"
)

// A DirConfig is a directory level configuration file which determines how the files in that directory are handled.
// It is typically found in files named _dirtempl.yml (see ConfigLoader).
type DirConfig struct {
	// Files maps the filenames to their FileEntries
	Files map[string]FileEntry `yaml:"files,omitempty"`
}

// A FileEntry specifies how the file it applies to is handled when encountered in a template.
type FileEntry struct {
	// Ignored: skips file; not included at destination (other options are ignored)
	Ignored bool `yaml:"ignore,omitempty"`
	// DestinationNames: sets names of resulting files
	DestinationNames []string `yaml:"dest,omitempty"`
	// TemplatedContent: sets weather it contains a template (overrides TemplateEngine.IsTemplate)
	TemplatedContent string `yaml:"templated,omitempty"`
}

func entryByNames(cfg *DirConfig, names ...string) FileEntry {
	if cfg == nil || cfg.Files == nil {
		return FileEntry{}
	}

	for _, n := range names {
		e, ok := cfg.Files[n]
		if ok {
			return e
		}
	}

	return FileEntry{}
}

func parseBool(s string, def bool) (bool, error) {
	switch strings.ToLower(s) {
	case "true", "yes", "on", "t", "y":
		return true, nil
	case "false", "no", "off", "f", "n":
		return false, nil
	case "auto", "":
		return def, nil
	default:
		return def, errors.New("parsing " + strconv.Quote(s) + ": invalid syntax: must be one of true, false, auto or other valid value")
	}
}
